<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170321_215739_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey()->notNull(),
            'username' => $this->string(255)->defaultValue(null),
            'password' => $this->string(255)->defaultValue(null),
            'email' => $this->string(255)->defaultValue(null),
            'phonel' => $this->string(255)->defaultValue(null),
            'role_id' => $this->smallInteger(8)->defaultValue(1),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m170321_215739_create_user_table cannot be reverted.\n";
        $this->dropTable('user');
    }
}
