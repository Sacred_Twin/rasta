<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m170419_124511_create_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('contact', [
            'id' => $this->primaryKey()->notNull(),
            'email' => $this->string(255)->defaultValue(null),
            'phonel' => $this->string(255)->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time()),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m170419_124511_create_contact_table cannot be reverted. \n";
        $this->dropTable('contact');
    }
}
