<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Buy';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="wrap">
    <div class="container" style="background: #d6d6d6">
        <p><h1 align="center">КУПИТЕ НЕДВИЖИМОСТЬ В ДНЕПРЕ НА</h1></p>
        <p><h1 align="center">10% ВЫГОДНЕЕ ОСТАЛЬНЫХ</h1></p><br><br>
        <div class="col-xs-4">Самый широкий ассортимент недвижимости в Днепре</div>
        <div class="col-xs-4">Результат работы, направленный на вашу выгоду</div>
        <div class="col-xs-4">Опыт переговоров, отточенный непрерывной практикой</div>
        <br><br><br><br><br><br>
        <p align="center">Хотите хорошо сэкономить? Обращайтесь к агентам АН "НОВЫЕ ТРАДИЦИИ"!</p>
        <p align="center"><a href="" class="btn btn-danger">СДЕЛАТЬ ВЫГОДНУЮ ПОКУПКУ!</a></p>
    </div>
    <div class="container">
        <p><h3 align="center">ПРИ СОТРУДНИЧЕСТВЕ С НАМИ ВЫ ПОЛУЧИТЕ:</h3></p>
        <br><br><br>
        <div class="col-xs-6"><img src="/images/mountain1.jpg"></div>
        <div class="col-xs-6">
            <p><b>Экономию вашего бюджета</b></p>
            <p>Сделаем ли мы вашу покупку выгодной? Несомненно! По
                статистике наши клиенты приобретают свою недвижимость
                на 10% дешевле (средний показатель), в сравнении с теми,
                кто совершает покупку без нас. </p><br>
            <p>Почему так происходит? Это результат образцового знания
                объектов в продаже, умноженное на непрерывное
                совершенствование профессиональных навыков на практике.</p><br>
            <p align="center"><a href="" class="btn btn-danger">ХОЧУ УЗНАТЬ, КАК СЭКОНОМИТЬ!</a></p>
        </div>
    </div>
    <div class="container">
        <div class="col-xs-6">
            <p><b>Ассортимент недвижимости — наше
                    преимущество</b></p>
            <p>При покупке вы всегда уверены, что покупаете лучшее? Или
                есть сомнения, что посмотрели не всё, что не увидели
                самых выгодных предложений? Только не у нас!  </p><br>
            <p>У нас вы получите не «самый большой выбор объектов
                недвижимости», а <b>выбор из абсолютно всех объектов</b>,
                находящихся в продаже. Чувствуете разницу?!</p><br>
            <p align="center"><a href="" class="btn btn-danger">ХОЧУ УЗНАТЬ ВЕСЬ АССОРТИМЕНТ СЕЙЧАС!</a></p>
        </div>
        <div class="col-xs-6"><img src="/images/mountain2.jpg"></div>
    </div>
    <div class="container">
        <div class="col-xs-6"><img src="/images/mountain3.jpg"></div>
        <div class="col-xs-6">
            <p><b>Юридически защищенную сделку и никаких
                    вопросов в будущем!</b></p>
            <p>Мы предоставляем вам комплексную услугу, в том числе и
                юридическое сопровождение.  </p><br>
            <p>Порекомендуем лучшую схему оформления для
                максимального удовлетворения ваших пожеланий.
                Проверим квартиру, владельцев, подготовим
                необходимые документы, организуем сделку.</p><br>
            <p align="center"><a href="" class="btn btn-danger">ХОЧУ КУПИТЬ НЕДВИЖИМОСТЬ БЕЗОПАСНО!</a></p>
        </div>
    </div>
    <div class="container">
        <p><h3 align="center"><b>КАК ВЫ ПОЛУЧАЕТЕ ЛУЧШИЙ РЕЗУЛЬТАТ?</b></h3></p><br><br>
        <p align="center">Чтобы вы купили недвижимость, полностью подходящую под ваши требования,</p>
        <p align="center">мы свою работу строим на 3 китах:</p>
        <div class="col-xs12">
            <div class="col-xs-4"> Кит 1:   Взаимопонимание относительно
                ваших целей, задач, требований </div>
            <div class="col-xs-4"></div>
            <div class="col-xs-4">Кит 3: Мотивация — оценка вами
                результатов нашей работы</div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">Кит 2: Дисциплинированный мониторинг рынка</div>
            <div class="col-xs-4"></div>
        </div><br><br><br><br>

        <p align="center"><img src="/images/kityy.jpg"></p><br>
        <p><h3 align="center">ВАМ ТОЧНО ПОНРАВИТСЯ НАШ ПОДХОД И</h3></p>
        <p><h3 align="center">ПОЛУЧЕННЫЙ РЕЗУЛЬТАТ</h3></p><br><br>
        <div class="col-xs-12">
            <div class="col-xs-4">
                <p align="center"><img src="/images/kit1.jpg"></p><br>
                <p><b>Взаимопонимание между
                        нами так важно?</b></p><br>
                <p>Вы уже попадали в ситуацию, когда
                    впустую теряли время и самообладание,
                    когда вас не понимали?</p><br>
                <p>Если ответ ДА, то вы знаете, как важно
                        найти риелтора, понимающего вас с
                        полуслова.  </p><br>
                <p>А если НЕТ, работая с нами, и не
                        узнаете</p>

            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/kit2.jpg"></p><br>
                <p><b>Что дает знание рынка? </b></p>
                <p>Гарантию эффективного выбора
                    объекта  по  лучшей цене.</p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/kit3.jpg"></p><br>
                <p><b>Почему ваше мнение о нас —
                        мощнейшая мотивация?</b></p>
                <p>Если мы добьемся от вас восторга по
                    окончании сделки, то обязательно
                    получим ваши рекомендации для
                    друзей, родственников, знакомых. </p>
                <p>Это приведет нас к увеличению
                    заказов, что возможно только в
                    случае идеального выполнения нами
                    взятых на себя обязательств. </p>
            </div>
        </div>
    </div>
    <br>
    <p align="center"><a href="" class="btn btn-danger">ОТЛИЧНО, НАЧИНАЕМ ПОКУПКУ ПРЯМО СЕЙЧАС!</a></p>
    <br>
    <div class="container">
        <p><h3 align="center">ВПЕЧАТЛЯЮЩАЯ СТАТИСТИКА ПОКУПОК</h3></p><br><br><br>
        <div class="col-xs-12">
            <div class="col-xs-4">
                <p align="center"><img src="/images/stat_1.jpg"></p>
                <p align="center">объектов купили клиенты
                    с нашей помощью в 2016</p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/stat_2.jpg"></p>
                <p align="center">столько наши клиенты потратили
                    на недвижимость в 2016 году  </p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/stat_3.jpg"></p>
                <p align="center">столько мы сэкономили
                    своим клиентам в 2016 году</p>
            </div>
        </div>

    </div><br>
    <div class="container">
        <p><h3 align="center">ВПЕЧАТЛЯЮЩАЯ СТАТИСТИКА ПОКУПОК</h3></p>
        <br><br><hr>
        <p><h3 align="center">КВАРТИРЫ</h3> </p>
        <section class="slider-index">
            <div id="carousel-index" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
<!--                <ol class="carousel-indicators">-->
<!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                </ol>-->

                <!-- Wrapper for slides -->

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/first.jpg" class="col-xs-12 image" alt="...">
                    </div>

                </div>
                <!--
                                                    </div>
                                                    <!-- Controls -->
                <a class="left carousel-control" href="#carousel-index" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-index" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
        <br><br><hr><br><br><hr>
        <p><h3 align="center">ДОМА</h3> </p>
        <section class="slider-index">
            <div id="carousel-index" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <!--                <ol class="carousel-indicators">-->
                <!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
                <!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
                <!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
                <!--                    <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
                <!--                </ol>-->

                <!-- Wrapper for slides -->

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/first.jpg" class="col-xs-12 image" alt="...">
                    </div>

                </div>
                <!--
                                                    </div>
                                                    <!-- Controls -->
                <a class="left carousel-control" href="#carousel-index" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-index" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section><br><br><hr>
    </div>
    <p align="center"><p align="center"><a href="" class="btn btn-danger">ХОЧУ УЗНАТЬ ОБО ВСЕХ ВАРИАНТАХ!</a></p></p>
    <br><br>
    <div class="container">
        <p><h3 align="center">МНЕНИЕ НАШИХ КЛИЕНТОВ И ИХ РЕКОМЕНДАЦИИ</h3></p><br>
        <p align="center">Обратите внимание на наш опыт в торговле недвижимостью. За 7 лет работы мы сделали
            счастливыми тысячи семей, которые оставили отзывы благодарности для будущих клиентов АН
            "Новые традиции".</p>
        <div class="col-xs-12" style="margin-top: 20px">
            <div class="col-xs-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe><p align="center">Семья ххххххх, покупка 2­комнатной квартиры в таком­то
                    районе</p></div>
            <div class="col-xs-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe><p align="center"> Семья ххххххх, покупка 2­комнатной квартиры в таком­то
                    районе</p></div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-3"></div>
            <div class="col-xs-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe><p align="center"> Семья ххххххх, покупка 2­комнатной квартиры в таком­то
                    районе</p></div><br>
            <div class="col-xs-3"></div>
        </div>
    </div><br>
    <p align="center"><a href="" class="btn btn-danger">ХОЧУ ТАК ЖЕ!</a></p>
    <div class="container">
        <p><h3 align="center"><b>НАША <u>ВЕСЕННЯЯ</u> АКЦИЯ</b></h3></p><br><br>
        <div class="container" style="background: lightskyblue">
            <div class="col-xs-7">
                <p align="center" style="margin-top: 50px">Весной своим клиентам мы предлагаем акцию:</p>
                <p align="center">при заказе наших услуг получите скидку</p>
                <p align="center"><b>на услуги нотариуса в размере 1 100 гривен!</b></p>
                <div class="">
                    <table align="center" border="0">
                        <tr>
                            <td align="center">2</td>
                            <td align="center">:</td>
                            <td align="center">2</td>
                            <td align="center">9</td>
                            <td align="center">:</td>
                            <td align="center">1</td>
                            <td align="center">1</td>
                        </tr>
                        <tr>
                            <td align="center">месяцев</td>
                            <td align="center"></td>
                            <td colspan="2" align="center">дней</td>
                            <td align="center"></td>
                            <td colspan="2" align="center">Часов</td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-5">
                <form action="" method="post" style="background: whitesmoke; margin-top: 10px">
                    <p align="center">Введите свои данные в форму ниже, и наш</p>
                    <p align="center">менеджер свяжется с вами, чтобы</p>
                    <p align="center">зафиксировать скидку и обсудить покупку:</p>
                    <input type="text" name="name" placeholder="Ваше имя:" class="col-xs-12">
                    <input type="text" name="phone" placeholder="Ваш телефон:" class="col-xs-12">
                    <p align="center"><button type="submit" name="submit-btn" class="btn btn-danger col-xs-12">ПОЛУЧИТЬ СКИДКУ НА УСЛУГИ НОТАРИУСА!</button></p>
                    <p align="center"><i>Мы свяжемся с вами в течение часа</i></p>
                </form>
            </div>

        </div>
    </div>
    <p><h3 align="center">ОСТАЛИСЬ ВОПРОСЫ?   <u><a href="">ЗАДАЙТЕ ИХ НАШЕМУ МЕНЕДЖЕРУ!</a></u></h3></p>
</div>