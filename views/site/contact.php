<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap">
    <div class="container">
        <section style="background: rgba(128, 128, 128, 0.42); height: 270px">
            <p><h1 align="center" style="padding-top: 110px">Photo</h1></p>
        </section>
    </div>
    <p><h3 align="center">ДАВАЙТЕ ОБСУДИМ ВОПРОСЫ ПО ТЕЛЕФОНУ</h3> </p>
    <p><h3 align="center"><b>+38 (067) 315­52­00</b></h3></p>
    <p align="center">Либо закажите обратный звонок. Мы вам перезвоним в ближайшее время!</p><br>
    <p align="center"><a href="" class="btn btn-danger"><b>ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК!</b></a></p>
    <br><br>
    <div class="container">
        <p><h3 align="center">НАПИШИТЕ НАМ НА E­MAIL ИЛИ В СПЕЦИАЛЬНОЙ ФОРМЕ</h3> </p>
        <p align="center">Наша почта для общих вопросов: <b><u>rubikon272@gmail.com</u></b></p>
        <br><br>


        <div class="col-xs-12" style="background: grey; margin-bottom: 50px">

            <form action="" method="post" style="margin-top: 40px">

                    <div class="col-xs-1"></div>
                    <div class="col-xs-5">
                        <p><input type="text" placeholder="Ваше имя:" class="col-xs-12"></p>
                        <p><input type="email" placeholder="Ваша почта:" class="col-xs-12"></p>
                    </div>

                    <div class="col-xs-5">
                        <input type="text" placeholder="Ваше сообщение или вопрос:" class="col-xs-12">
                    </div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-12" style="padding-top: 20px">
                        <p align="center"><button type="submit" class="btn btn-danger"><b>ОТПРАВИТЬ ЗАПРОС!</b></button></p>
                    </div>

            </form>

        </div>
        <p><h3 align="center">ИЛИ ПРИХОДИТЕ К НАМ ЛИЧНО!</h3></p>
        <br>
    </div>



    <div class="container">

    </div>
</div>

