<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;
?>





<div class="wrap">
    <div class="container" style="background: rgba(128, 128, 128, 0.4); padding-bottom: 20px">
                <div class="">
                    <p><b>Вы желаете - мы воплощаем!</b></p>
                    <p><h1><b>ЭКСПЕРТ No1</b></h1></b>
                    <p><h1><b>В ТОРГОВЛЕ</b></h1></p>
                    <p><h1><b>НЕДВИЖИМОСТЬЮ</b></h1></p>
                </div>
                <br><br>
                <div class="container">
                <div class="col-xs-4">
<!--                    <section class="slider-index">-->
<!--                        <div id="carousel-index" class="carousel slide" data-ride="carousel">-->
<!---->
<!--                            <!-- Indicators -->
<!--                            <ol class="carousel-indicators">-->
<!--                                <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                                <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                                <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                                <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                            </ol>-->
<!---->
<!--                            <!-- Wrapper for slides -->
<!---->
<!--                            <div class="carousel-inner" role="listbox">-->
<!--                                <div class="item active">-->
<!--        <!--                            <img src="/images/first.jpg" class="col-xs-12 image" alt="...">-->
<!--                                    <p class="image" id="carousel-index"><h3 align="center">Увеличиваем выгоду каждого клиента в-->
<!--                                        среднем на 2 500$ </h3></p>-->
<!--                                </div>-->
<!--                                <div class="item active">-->
<!--                                    <!--                            <img src="/images/first.jpg" class="col-xs-12 image" alt="...">--
<!--                                    <p class="image" id="carousel-index"><h3 align="center">Увеличиваем выгоду каждого клиента в-->
<!--                                        среднем на 2 500$ </h3></p>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!---->
<!--                            <!-- Controls -->
<!--                            <a class="left carousel-control" href="#carousel-index" role="button" data-slide="prev">-->
<!--                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
<!--                                <span class="sr-only">Previous</span>-->
<!--                            </a>-->
<!--                            <a class="right carousel-control" href="#carousel-index" role="button" data-slide="next">-->
<!--                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
<!--                                <span class="sr-only">Next</span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </section>-->

                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block img-fluid" src="..." alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="..." alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="..." alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>



                </div>
                </div><br><br>
                <div class="col-xs-12">
                    <div class="col-xs-4">
                        <a href="" class="btn btn-danger">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ ПО НЕДВИЖИМОСТИ</a>
                    </div>

                </div>
    </div>
    <div class="container">
        <div class="col-xs-12" style="padding-bottom: 20px">
            <p><h3 align="center"><b>НЕОСПОРИМЫЕ ФАКТЫ ОБ АН "НОВЫЕ ТРАДИЦИИ"</b></h3></p><br><br>
            <div class="col-xs-4">
                <p align="center"><img src="/images/7years.jpg"></p>
                <p align="center">опыт компании в операциях с недвижимостью</p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/91.jpg"></p>
                <p align="center">наших клиентов рекомендуют нас другим</p>
            </div>
            <div class="col-xs-4"> 
                <p align="center"><img src="/images/9millions.jpg"></p>
                <p align="center">на эту сумму купили и продали недвижимость клиенты с нашей помощью </p>
                
            </div>
        </div>
        <div><p><h3 align="center"><b>НАША <u>ВЕСЕННЯЯ</u> АКЦИЯ</b></h3></p> </div>
        <div class="container" style="background: lightskyblue; padding-bottom: 30px; margin-bottom: 50px">
            <div class="col-xs-7">
                <p align="center" style="padding-top: 70px">Весной своим клиентам мы предлагаем акцию:</p>
                <p align="center">при заказе наших услуг получите скидку</p>
                <p align="center"><b>на услуги нотариуса в размере 1 100 гривен!</b></p>
                <div class="">
                    <table align="center" border="0">
                        <tr>
                            <td align="center">2</td>
                            <td align="center">:</td>
                            <td align="center">2</td>
                            <td align="center">9</td>
                            <td align="center">:</td>
                            <td align="center">1</td>
                            <td align="center">1</td>
                        </tr>
                        <tr>
                            <td align="center">месяцев</td>
                            <td align="center"></td>
                            <td colspan="2" align="center">дней</td>
                            <td align="center"></td>
                            <td colspan="2" align="center">Часов</td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-5">
                <form action="" method="post" style="background: whitesmoke; margin-top: 40px">
                    <p align="center">Введите свои данные в форму ниже, и наш</p>
                    <p align="center">менеджер свяжется с вами, чтобы</p>
                    <p align="center">зафиксировать скидку и обсудить ваш вопрос:</p>
                    <input type="text" name="name" placeholder="Ваше имя:" class="col-xs-12">
                    <input type="text" name="phone" placeholder="Ваш телефон:" class="col-xs-12">
                    <p align="center"><button type="submit" name="submit-btn" class="btn btn-danger">ПОЛУЧИТЬ СКИДКУ НА УСЛУГИ НОТАРИУСА!</button></p>
                    <p align="center"><i>Мы свяжемся с вами в течение часа</i></p>
                </form>
            </div>

        </div>
        <div class="container col-xs-12" style="margin-bottom: 40px">
            <p><h3 align="center"><b>ПОЧЕМУ ВАМ ЛУЧШЕ ДОВЕРИТЬ ПОКУПКУ/ПРОДАЖУ</b></h3></p>
            <p><h3 align="center"><b>НЕДВИЖИМОСТИ АГЕНТАМ АН "НОВЫЕ ТРАДИЦИИ"</b></h3></p><br><br>
            <div class="col-xs-8">
                <p><h2 align="center">Самостоятельно Вы:</h2></p>
                <table style="border-collapse: collapse; border-right: 3px solid black;">

                    <tr>
                        <td><h3><b>1.</b></h3> С Чего начать?</td>
                        <td><h3><b>2.</b></h3> Взвешиваете, решить свой вопрос самостоятельно или обратиться в агентство, принимаете решение сэкономить и сделать всё самому.</td>
                        <td><h3><b>3.</b></h3> Собираете информацию из открытых источников, пытаетесь узнать текущие цены на рынке</td>
                    </tr>
                    <tr>
                        <td><h3><b>6.</b></h3> Долго ведете переговоры и торг, часто с теми, кто ваших аргументов не слышит. Это раздражает!  </td>
                        <td><h3><b>5.</b></h3> Готовите презентацию, ищете покупателей. Находите, но далеко не всех. Те, кого нашли, предлагают мало либо уходят молча.  </td>
                        <td><h3><b>4.</b></h3> Ищете недвижимость. Сомневаетесь, не пропустили ли идеальный объект. Находите достаточно много, но далеко не всё. То, что нашли, либо не подходит, либо дорого.</td>
                    </tr>
                    <tr>
                        <td><h3><b>7.</b></h3> Честно принимаете
                            большое количество
                            звонков от посредников,
                            отвечаете на
                            одинаковые вопросы,
                            действий много — толку
                            мало, результата —
                            ноль.  </td>
                        <td><h3><b>8.</b></h3> Не удовлетворившись
                            результатами,
                            потеряв некоторое
                            время, решаете
                            обратиться в
                            агентство
                            недвижимости. </td>
                        <td><h3><b>9.</b></h3> Внимательно готовите
                            сделку и документы в
                            случае, если решились
                            принять то, что нашли,
                            подозревая, что можно
                            было сделать и выгоднее.</td>
                    </tr>
                </table>
            </div>
            <div clas="col-xs-4" >
                <p><h2 align="center">Что предлагаем мы:</h2></p>
                <table>
                    <tr><td><h3><b>1.</b></h3> Обратиться за
                        консультацией к нам, для
                            решения вашего вопроса: <p><a href="" name="recall" class="btn btn-danger">Обратиться!</a> </p></td></tr>
                    <tr><td><h3><b>2.</b></h3> Мы, изучив ваш вопрос,
                        предложим лучшие решения,
                        используя свои базы данных,
                            методики и навыки.</td></tr>
                    <tr><td><h3><b>3.</b></h3> Осуществим комплексно
                        необходимые операции с
                        недвижимостью с
                        наибольшей выгодой для
                        вас, безопасно и
                            оперативно.</td></tr>
                </table>
            </div>
        </div>
        <div>
            <p align="center">Хотите, чтобы ваша покупка или продажа прошла эффективно и просто?</p>
            <p align="center">Обратитесь к нашим профессионалам!</p>
            <p align="center">
                <a href="" id="salle" name="salle-btn" class="btn btn-danger">Продать недвижимость</a>
                <a href="" id="buy" name=buy-btn" class="btn btn-default">Купить недвижимость</a>
                <br>
                <br>
                <br>

            </p>
        </div>
        <div class="container">
            <p><h3 align="center"><b>СЕРЬЕЗНЫЕ ПРЕИМУЩЕСТВА И ОТИЛЧНЫЕ ПЕРСПЕКТИВЫ</b></h3></p>
            <br>
            <br>
            <table>
                <tr>
                    <td align="center"><img src="/images/smile_blue.jpg"> </td>
                    <td align="center"><img src="/images/smile_blue.jpg"> </td>
                    <td align="center"><img src="/images/smile_blue.jpg"> </td>
                </tr>
                <tr>
                    <td align="center"><b>Вы покупаете или продаете по
                            максимально выгодной цене</b></td>
                    <td align="center"><b>Вы совершаете свои
                            операции с недвижимостью
                            эффективно и быстро </b></td>
                    <td align="center"><b>Ваша сделка безопасная и
                            юридически чистая</b></td>
                </tr>
                <tr>
                    <td align="center">Мы гарантируем, что наш
                        результат будет выгоднее, чем
                        без нас!</td>
                    <td align="center">Когда другие только
                        разворачивают рекламную
                        кампанию, мы можем предложить
                        готовых покупателей и продавцов.</td>
                    <td align="center">Мы сотрудничаем только с
                        профессионалами: нотариусами,
                        юристами, оценщиками, которые
                        доказали свои качества на практике.</td>
                </tr>
                <tr>
                    <td align="center"><img src="/images/smile_blue.jpg"> </td>
                    <td align="center"><img src="/images/smile_blue.jpg"> </td>
                    <td align="center"><img src="/images/smile_blue.jpg"> </td>
                </tr>
                <tr>
                    <td align="center"><b>Вы эффективно расходуете
                            свое время </b></td>
                    <td align="center"><b>В вашем распоряжении все
                            наши базы</b></td>
                    <td align="center"><b>Вы постоянно на связи со
                            своим агентом</b></td>
                </tr>
                <tr>
                    <td align="center">Мы берем на себя все обязанности
                        комплексно. Вы тратите на общение 3
                        минуты в день с нашим специалистом
                        вместо часа с большим количеством
                        посредников.</td>
                    <td align="center"> Мы предлагаем вам всю
                        имеющуюся у нас информацию для
                        правильного принятия решения:
                        базы покупателей и продавцов,
                        проданных объектов и цен.</td>
                    <td align="center">Вы всегда в курсе происходящих
                        дел и знаете, как работает агент
                        над вашей проблемой, какие
                        результаты уже есть.</td>
                </tr>
            </table><br><br>
        </div>
        <div class="container" style="background: lightskyblue; padding-top: 40px">
            <div class="col-xs-7">
                    <p align="center">Воспользуйтесь всеми преимуществами сотрудничества с нами.</p>
                    <p align="center">Оставьте свой номера телефона, и наш менеджер поможет вам решить проблему покупки/продажи недвижимости.</p>
            </div>
            <div class="col-xs-5">
                <form action="" method="post" style="background: whitesmoke;">
                    <input type="text" name="phone" placeholder="Ваш телефон:" class="col-xs-12">
                    <p align="center"><button type="submit" name="submit-btn" class="btn btn-danger col-xs-12">Воспользоваться</button></p>
                </form>
                <p align="center">Мы свяжмся с Вами в течении часа</p>
            </div>

        </div>
        <div class="container" style="margin-top: 60px">
            <p><h3 align="center"><b>АН "НОВЫЕ ТРАДИЦИИ" — ЭТО ПРОСТОЕ РЕШЕНИЕ</b></h3></p>
            <p><h3 align="center"><b>СЛОЖНЫХ ВОПРОСОВ</b></h3></p><br>
            <p align="center"> Наши менеджеры проведут для вас грамотную консультацию по любому вопросу, </p>
            <p align="center">связанному с недвижимостью, абсолютно бесплатно.  </p>
            <br><br>
            <div class="col-xs-12" style="margin-bottom: 60px">
                <div class="col-xs-6">
                    <p align="center"><img src="/images/mountain.jpg"></p>
                    <p align="center">Хотите купить недвижимость в Днепре по
                        наиболее выгодной цене?</p>
                    <p align="center"><a href="" class="btn btn-danger">ДА! ХОЧУ <u>КУПИТЬ</u> НЕДВИЖИМОСТЬ!</a></p>
                </div>
                <div class="col-xs-6">
                    <p align="center"><img src="/images/mountain.jpg"></p>
                    <p align="center">Хотите продать недвижимость в Днепре по
                        наиболее выгодной цене? </p>
                    <p align="center"><a href="" class="btn btn-danger">ДА! ХОЧУ <u>ПРОДАТЬ</u> НЕДВИЖИМОСТЬ!</a></p>
                </div>
            </div>

            <p><h3 align="center">ПРЕИМУЩЕСТВА РАБОТЫ С НАМИ УЖЕ</h3></p>
            <p><h3 align="center">ОЦЕНИЛИ БОЛЕЕ 1 000 ЧЕЛОВЕК</h3></p><br><br>
            <p align="center"> Все мнения клиентов мы не имеем возможности разметить. Представляем лишь некоторые отзывы:</p>
        </div><br><br>
        <div class="">
            <div class="col-xs-12">
                <section class="slider-index">
                    <div id="carousel-index" class="carousel slide" data-ride="carousel">

                        <!-- Indicators -->
<!--                        <ol class="carousel-indicators">-->
<!--                            <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                            <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                            <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                            <li data-target="#carousel-index" data-slide-to="0" class="active"></li>-->
<!--                        </ol>-->

                        <!-- Wrapper for slides -->

                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/first.jpg" class="col-xs-12 image" alt="...">
                            </div>

                        </div>

                        <!-- Controls -->

                        <a class="left carousel-control" href="#carousel-index" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-index" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
            </div>
        </div>
        <div>
            <p align="center"><a href="" class="btn btn-danger">ХОЧУ ТАКЖЕ!</a></p>
        </div>
        <div class="container">
            <p><h3 align="center">ВОПРОСЫ ОТ НАШИХ КЛИЕНТОВ</h3></p>
            <br><br>
            <div class="col-xs-12">
                <div class="col-xs-1"></div>
                <div class="col-xs-4">
                    <p align="left" ><b>Но я ведь могу всё сделать самостоятельно...</b></p>
                    <p align="left" style="padding-bottom: 70px">Можете. Не отрицаем. Останется только пара вопросов... За сколько продадите/купите? Сколько при этом потратите времени? Кто сможет вам гарантировать чистоту сделки?</p>
                    <p align="left"><b>Я обращусь сразу в несколько АН, чтобы быстрее было...</b></p>
                    <p align="left">Вам не нужно об этом беспокоиться. Мы организовываем всех риелторов Днепра, и если у кого­либо есть клиент, он будет у вас на объекте. Вы не тратите свое время и силы, чтобы рассказать всё каждому из</p>
                </div>
                <div class="col-xs-2"></div>
                <div class="col-xs-4">
                    <p align="left"><b>Может, лучше в другое агентство? Там дешевле...</b></p>
                    <p align="left" style="padding-bottom: 30px">Мы бы могли ответить на это крылатым выражением: "За работу можно вообще не платить, если результат вас не интересует". Однако скажем только, что стоимость нашей услуг оправданна: даже с учетом размера наших услуг работать с нами выгодно.</p>
                    <p align="left"><b>Я боюсь ваших нотариусов, у меня есть свой...</b></p>
                    <p align="left">Мы работаем с разными нотариусами. Если вы уверены в честности вашего нотариуса, поможем оформить сделку через него. Но тогда мы не сможем вам гарантировать чистоту сделки и отсутствие проблем в будущем.</p>
                </div>
                <div class="col-xs-1"></div>

            </div>
        </div>
        <br><br>
        <p><h3 align="center">ОСТАЛИСЬ ВОПРОСЫ? <u><a href="">    ЗАДАЙТЕ ИХ НАШЕМУ МЕНЕДЖЕРУ!</a></u></h3></p>
    </div>
</div>
