<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Sell';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="wrap">
    <div class="container bg_font_2">
        <p><h1 align="center"><b>ПРОДАЙТЕ НЕДВИЖИМОСТЬ В ДНЕПРЕ ЗА</b></h1></p>
        <p><h1 align="center"><b>8 НЕДЕЛЬ ПО <u>САМОЙ</u> ВЫГОДНОЙ ЦЕНЕ</b></h1></p>
        <br>
        <br>
        <br>
        <div class="col-xs-4">Максимально высокая
            цена на ваш объект </div>
        <div class="col-xs-4">Большая база готовых
            покупателей для вас</div>
        <div class="col-xs-4">Эффективное использование
            вашего времени </div>
        <br><br><br><br><br><br>
        <p align="center"><a href="" class="btn btn-danger">УЗНАТЬ, КАК ПРОДАТЬ НЕДВИЖИМОСТЬ ВЫГОДНО!</a></p>
    </div>
    <div class="container">
        <p><h3 align="center"><b>С АН "НОВЫЕ ТРАДИЦИИ" ВЫ ДОБЬЕТЕСЬ</b></h3></p>
        <p><h3 align="center"><b>ЛУЧШИХ РЕЗУЛЬТАТОВ, ПОТОМУ ЧТО...</b></h3></p>
        <br>
        <p align="center">Наша работа кардинально отличается от традиционного поиска</p>
        <p align="center">покупателя. Ждать у моря погоды не наш метод!</p>
        <br>
        <table class="col-xs-12" border="0" style="margin-bottom: 60px">
            <tr>
                <td></td>
                <td><h4 align="center" align="center"  style="border-collapse: collapse; border-right: 3px solid black;">Традиционный подход</h4></td>
                <td></td>
                <td><h4 align="center" align="center"> Что предлагаем мы</h4></td></td>
            </tr>
            <tr>
                <td align="center"><img src="/images/1.jpg"></td>
                <td align="center"  style="border-collapse: collapse; border-right: 3px solid black;">Агент принимает ваш объект в работу</td>
                <td align="center"><img src="/images/1.jpg"></td>
                <td align="center">В нашей базе уже есть готовый покупатель, осталось презентовать ему ваш объект</td>
            </tr>
            <tr>
                <td align="center"><img src="/images/2.jpg"></td>
                <td align="center"  style="border-collapse: collapse; border-right: 3px solid black;">Начинает рекламировать ваш объект</td>
                <td align="center"><img src="/images/2.jpg"></td>
                <td align="center">Параллельно разворачиваем рекламную
                    кампанию для увеличения вероятности успеха</td>
            </tr>
            <tr>
                <td align="center"><img src="/images/3.jpg"></td>
                <td align="center"  style="border-collapse: collapse; border-right: 3px solid black;">Ждет потенциальных клиентов</td>
                <td align="center"><img src="/images/3.jpg"></td>
                <td align="center">Получаем несколько предложений о покупке,
                    выбираем лучшее  </td>
            </tr>
            <tr>
                <td align="center"><img src="/images/4.jpg"></td>
                <td align="center"  style="border-collapse: collapse; border-right: 3px solid black;">Заставляет ожидать вас, пока не оказывается,
                    что так рекламироваться вы могли и сами</td>
                <td align="center"><img src="/images/4.jpg"></td>
                <td align="center">В среднем закрываем сделку продажи вашей
                    недвижимости уже через 8 недель</td>
            </tr>
            <tr>
                <td  colspan="2" align="right"><img src="/images/x.jpg"></td>
                <td></td>
                <td colspan="2" align="right"><img src="/images/v.jpg"></td>
                <td></td>
            </tr>

        </table>

        <p><h3 align="center"><b>ВАШ МАКСИМУМ ПОКУПАТЕЛЕЙ</b></h3> </p>
        <br>
        <br>
        <p align="center">Для эффективного продвижения вашей недвижимости мы готовим качественную презентацию,
            которую адресуем:</p><br>
        <div class="col-xs-12" style="padding-bottom: 30px">
            <div class="col-xs-4">
                <p align="center"><img src="/images/circle.jpg"></p>
                <p align="center"><b>Вашим соседям и жителям
                        микрорайона</b></p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/circle.jpg"></p>
                <p align="center"><b>Десяткам интернет­ресурсов
                        и печатных изданий</b></p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/circle.jpg"></p>
                <p align="center"><b>Сотням наших коллег для
                        привлечения их покупателей</b></p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/circle.jpg"></p>
                <p align="center"><b>3 тысячам клиентов из
                        нашей базы</b></p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/circle.jpg"></p>
                <p align="center"><b>Десяткам тысяч владельцев
                        электронной почты и
                        мобильных телефонов</b></p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/circle.jpg"></p>
                <p align="center"><b>Миллионам пользователей
                        социальных сетей facebook, vk,
                        twiter, instagram, viber и пр.</b></p>
            </div>
        </div>
        <br>
        <br>
        <br>
        <p align="center">Это далеко не полный список действий по привлечению клиентов. Отметим, что для осуществления этих</p>
        <p align="center">мероприятий трудится команда профессионалов по 8 часов в день, 7 дней в неделю!</p>
        <br>
        <p align="center"><a href="" class="btn btn-danger">ХОЧУ ПОЛУЧИТЬ ПОТОК КЛИЕНТОВ ПРЯМО СЕЙЧАС! </a></p>
    </div>
    <div class="container bg_font_3">
        <p><h3 align="center"><b>ПОЧЕМУ ВАМ БУДЕТ ПРОСТО СОТРУДНИЧАТЬ С НАМИ</b></h3> </p>
        <br><br><br>
        <p align="center">АН "Новые традиции" — это максимальное погружение в ваш вопрос! </p>
        <br>
        <p align="center">Наши сотрудники так же сильно любят свою работу, как и уважают наших клиентов. Они компетентны,
            вежливы, организованны и сделают всё, чтобы ваша задача была решена качественно и точно в срок.
            Для этого они детально изучают вашу ситуацию в целом и конкурентную недвижимость в частности.
            Затем предлагают вам оптимальное решение вопроса.</p>
    </div>
    <div class="container">
        <p><h3 align="center"><b>КОМПЛЕКСНЫЙ ПОДХОД К ДЕЛУ</b></h3> </p>
        <br>
        <p align="center">Продажа лишь верхушка айсберга! 90% нашей работы остается невидимой для вас.</p>
        <p align="center">Желаемые для вас результаты дает только комплексный подход к решению задач.</p>
        <br>
        <div class="col-xs-12">
            <table style="border-spacing: 20px; border-collapse: separate;" width="100%">
                <tr>
                    <td align="center" style="border: solid 1px; padding: 15px">
                        <p><img src="/images/star.jpg"></p>
                        <p>Вы связываетесь с нами.</p>
                            <p>Мы обсуждаем с вами
                            план дальнейших
                            действий.</p>
                        <p>
                        <a href="#" class="btn btn-danger">СВЯЗАТЬСЯ!</a>
                        </p>
                    </td>
                    <td align="center" style="border: solid 1px; padding: 15px">
                        <p><img src="/images/star.jpg"></p>
                        <p>Определение
                            максимальной цены</p>
                    </td>
                    <td align="center" style="border: solid 1px; padding: 15px">
                        <p><img src="/images/star.jpg"></p>
                        <p>Предпродажная
                            подготовка</p>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border: solid 1px; padding: 15px">
                        <p><img src="/images/star.jpg"></p>
                        <p>Привлечение клиентов,
                            готовых купить вашу
                            недвижимость  </p>
                    </td>
                    <td align="center" style="border: solid 1px; padding: 15px">
                        <p><img src="/images/star.jpg"></p>
                        <p>Ведение переговоров,
                            торг</p>
                    </td>
                    <td align="center" style="border: solid 1px; padding: 15px">
                        <p><img src="/images/star.jpg"></p>
                        <p>Проверка чистоты и
                            организация сделки</p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="container">
        <p><h3 align="center"><b>ПОЧЕМУ ВАЖНО НЕ ОШИБИТЬСЯ С ЦЕНОЙ?</b></h3></p>
        <br>
        <div class="col-xs-12" style="padding-top: 20px; padding-bottom: 60px">
            <div class="col-xs-6">
                <p align="center">Если цена занижена...</p>
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center">Вы теряете деньги, а мы — новых
                    клиентов, которым вы нас не
                    порекомендуете. Пострадает наша
                    репутация, и в результате нам
                    придется уйти с рынка. Наша
                    мотивация сделать лучшую цену —
                    ваши рекомендации.</p>
            </div>
            <div class="col-xs-6">
                <p align="center">Если цена завышена...</p>
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center">Желающих купить вашу недвижимость не
                    будет. Все потенциальные покупатели
                    приобретут аналогичную недвижимость по
                    более привлекательной цене. В результате
                    цену придется снизить, при этом потеряв
                    самое выгодное время для продажи —
                    первый месяц старта продаж.</p>
            </div>
        </div>
        <p style="padding-top: 20px"><h3 align="center"><b>КАК МЫ ОПРЕДЕЛИМ ДЛЯ ВАС САМУЮ ВЫГОДНУЮ</b></h3></p>
        <p><h3 align="center"><b>ЦЕНУ НА РЫНКЕ</b></h3></p>
        <br>
        <br>
        <div class="col-xs-12" style="padding-top: 20px; padding-bottom: 30px">
            <div class="col-xs-6">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center">Взвешиваем спрос на
                    ваш объект</p>
            </div>
            <div class="col-xs-6">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center">Анализируем рынок
                    аналогичных объектов</p>
            </div>
            <div class="col-xs-6">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center">Определяем конкурентов
                    на рынке</p>
            </div>
            <div class="col-xs-6">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center">Анализируем архив цен
                    проданной аналогичной
                    недвижимости</p>
            </div>
        </div>
        <p align="center"><a href="" class="btn btn-danger">ОПРЕДЕЛИТЬ ЦЕНУ МОЕЙ НЕДВИЖИМОСТИ!</a> </p>
        <br>
        <br>
        <br>
        <p><h3 align="center"><b>ПРЕДПРОДАЖНАЯ ПОДГОТОВКА И РЕКЛАМА</b></h3></p>
        <br>
        <p align="center">Чтобы добиться хороших результатов, наша команда ведет работу параллельно на трех фронтах:</p>
        <br>
        <div class="col-xs-12">
            <div class="col-xs-4">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center"><b>Подготовка недвижимости</b></p>
                <p>При подготовке вашего объекта к
                    продаже учтем все нюансы, которые
                    влияют на выбор покупателя: звук,
                    свет, запах, соседи и другие факторы.</p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center"><b>Подготовка документов</b></p>
                <p>Если ваши документы еще не
                    готовы или имеются неприятные
                    моменты борьбы с бюрократией
                    для получения нужных разрешений,
                    мы поможем и подготовим
                    необходимые документы к
                    продаже.</p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p align="center"><b>Привлечение клиентов</b></p>
                <p>Вы еще не забыли? У нас уже
                    есть готовый покупатель для вас!
                    Осталось только сообщить ему о
                    вашем объекте!  </p>
            </div>
        </div>
    </div>
    <div class="container img-big" style="padding-bottom: 30px; background: rgba(128,128,128,0.46)">
        <p><h3 align="center"><b>ВАЖНЫЙ ЭТАП ПЕРЕГОВОРОВ И ТОРГОВ</b></h3> </p>
        <br>
        <br>
        <br>
        <p align="center">Искусство торга и переговоров схоже с физической формой спортсмена: либо
            спортсмен тренируется, и результаты растут, либо навыки теряются... </p>
        <p align="center">Наши специалисты постоянно поддерживают отличную форму благодаря
            насыщенной практике: посещают тренинги, мастер­классы, семинары. </p>
    </div>
    <div class="container">
        <p><h3 align="center"><b>БЕЗОПАСНОСТЬ СДЕЛКИ ПРЕВЫШЕ ВСЕГО</b></h3> </p>
        <br>
        <p align="center">Заключение договора купли­продажи — финальная часть нашей работы! </p>
        <p align="center">Для чтобы обеспечить полную безопасность сделки, мы позаботимся еще о нескольких нюансах:</p>
        <br>
        <div class="col-xs-12" style="padding-bottom: 30px">
            <div class="col-xs-4">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p>Проверка в день сделки на
                    подлинность наличных средств,
                    принимаемых вами в оплату
                    недвижимости</p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p>Выбор квалифицированного
                    нотариуса для проведения
                    сделки</p>
            </div>
            <div class="col-xs-4">
                <p align="center"><img src="/images/img_salle.jpg"></p>
                <p>Присутствие на сделке
                    представителя банка и
                    банковских документов</p>
            </div>
        </div>
        <p><h3 align="center">МНЕНИЕ НАШИХ КЛИЕНТОВ И ИХ РЕКОМЕНДАЦИИ</h3></p>
        <br>
        <p align="center">Обратите внимание на наш опыт в торговле недвижимостью. За 7 лет работы мы сделали</p>
        <p align="center">счастливыми тысячи семей, которые оставили отзывы благодарности для будущих клиентов АН</p>
        <p align="center">"Новые традиции".</p><br><br>
        <div class="col-xs-12">
            <div class="col-xs-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe><p align="center">ФИО, должность, продажа 2­комнатной квартиры в таком­
                    то районе</p></div>
            <div class="col-xs-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe><p align="center">ФИО, должность, продажа 2­комнатной квартиры в таком­
                    то районе</p></div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-3"></div>
            <div class="col-xs-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe><p align="center">ФИО, должность, продажа 2­комнатной квартиры в таком­
                    то районе</p>
            <p align="center"><a href="" class="btn btn-danger">ХОЧУ ТАКЖЕ</a> </p>
            </div><br>
            <div class="col-xs-3"></div>
        </div>
    </div>
    <div class="container">
        <p><h3 align="center">НАША АКЦИЯ ДЛЯ ТЕХ, КТО ПРОДАЕТ СВОЮ</h3></p>
        <p><h3 align="center">НЕДВИЖИМОСТЬ В ДНЕПРЕ</h3></p>
        <br>
            <div class="container" style="background: lightskyblue">
                <div class="col-xs-7">
                    <p align="center" style="padding-top: 70px">Всем своим клиентам, которые собираются продавать</p>
                    <p align="center">свою недвижимость, мы дарим скидку</p>
                    <p align="center"><b>50% на услуги оценщика!</b></p>
                </div>
                <div class="col-xs-5">
                    <form action="" method="post" style="background: whitesmoke; margin-top: 10px">
                        <p align="center">Введите свои данные в форму ниже, и наш</p>
                        <p align="center">менеджер свяжется с вами, чтобы</p>
                        <p align="center">зафиксировать скидку и обсудить покупку:</p>
                        <input type="text" name="name" placeholder="Ваше имя:" class="col-xs-12">
                        <input type="text" name="phone" placeholder="Ваш телефон:" class="col-xs-12">
                        <p align="center"><button type="submit" name="submit-btn" class="btn btn-danger col-xs-12">ПОЛУЧИТЬ СКИДКУ 50% НА УСЛУГИ ОЦЕНЩИКА!</button></p>
                        <p align="center"><i>Мы свяжемся с вами в течение часа</i></p>
                    </form>
                </div>

            </div>
    </div>
    <p><h3 align="center">ОСТАЛИСЬ ВОПРОСЫ?   <u><a href="">ЗАДАЙТЕ ИХ НАШЕМУ МЕНЕДЖЕРУ!</a></u></h3></p>
</div>
