<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<header>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => 'АН Новые Традиции',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Главная', 'url' => ['/site/index']],
                ['label' => 'Продать', 'url' => ['/site/sell']],
                ['label' => 'Купить', 'url' => ['/site/buy']],
                ['label' => 'Контакты', 'url' => ['/site/contact']],
            ],
        ]);
        NavBar::end();
        ?>
</header>


  <?= $content ?>


<footer class="footer">
    <?php
    NavBar::begin([
        'brandLabel' => 'АН Новые Традиции',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-bottom',
            'id' => 'custom-footer'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => Yii::t('app', 'Продать'), 'url' => ['/site/sell']],
            ['label' => Yii::t('app', 'Купить'), 'url' => ['/site/buy']],
            ['label' => Yii::t('app', 'Контакты'), 'url' => ['/site/contact']],
        ]]);
    NavBar::end();

    ?>

</footer>

<!--------------------------------------------------------------------------------------------------------------------->
<script src="/js/slider.js"></script>
<!--------------------------------------------------------------------------------------------------------------------->




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
